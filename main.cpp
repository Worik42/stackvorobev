#include <iostream>
#include "Stack.h"

using namespace std;

int main() {
    int y;
    cout << "1 - int " << endl;
    cout << "2 - double " << endl;
    cout << "3 - char " << endl;
    cin >> y;

    if (y == 1) {

        int n;
        cout << "INT: " << endl;

        Stack<int> q;
        cout << "How many items? : ";
        cin >> n;
        q.pushArray(n);
        cout << q;
        if (q.size() > 1) {
            cout << "Delete last item" << endl;
            q.pop();

            cout << q;

            cout << "Extract item:{LAST}" << endl;

            int last = q.getLast();
            cout << "Extracted item:" << last << endl;

            cout << "Delete ALL item:" << endl;
            q.clear();
            cout << q;
        } else
            cout << "few elem" << endl;
    } else if (y == 2) {


        cout << "----DOUBLE: " << endl;
        int n;


        Stack<double> q;
        cout << "Skolko elem? : ";
        cin >> n;
        q.pushArray(n);
        cout << q;
        if (q.size() > 1) {
            cout << "Delete last item" << endl;
            q.pop();

            cout << q;

            cout << "Extract item:{LAST}" << endl;
            double last = q.getLast();
            cout << "Extracted item:" << last << endl;

            cout << "Delete ALL item:" << endl;
            q.clear();
            cout << q;
        } else
            cout << "few elem" << endl;

    } else if (y == 3) {

        cout << "----CHAR: " << endl;
        int n;


        Stack<char> q;
        cout << "Skolko elem? : ";
        cin >> n;
        q.pushArray(n);
        cout << q;
        if (q.size() > 1) {
            cout << "Delete last item" << endl;
            q.pop();

            cout << q;

            cout << "Extract item:{LAST}" << endl;
            char last = q.getLast();

            cout << "Extracted item:" << last << endl;

            cout << "Delete ALL item:" << endl;
            q.clear();
            cout << q;
        } else
            cout << "few elem" << endl;
    }
    return 0;
}
