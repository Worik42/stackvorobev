//
// Created by worik on 16.11.2017.
//

#ifndef STACK_STACK_H
#define STACK_STACK_H

#include <cctype>
#include <cstdlib>
#include <clocale>
#include <iostream>
#include <cstdio>

using namespace std;

#include <list>


template<class T>
class Stack {
private:
    std::list<T> container;

public:
    Stack() : container() {
    }

    void pop() { container.pop_back(); }

    void push(T lem) { container.push_back(lem); }

    bool empty() { return container.empty(); }

    int size() { return container.size(); }

    T getLast() {
        T cont = container.back();
        container.pop_back();
        return cont;
    }


    friend std::ostream &operator<<(ostream &vc, Stack<T> &stack) {
        if (stack.empty() != 1) //если не пусто
        {
            vc << "You stack" << endl;
            for (typename list<T>::iterator it = stack.container.begin(); it != stack.container.end(); ++it)
                vc << *it << endl;
        } else vc << "Stack is empty" << endl;
        return vc;
    }


    void clear() {
        for (; container.size() != 0;) {
            container.pop_back();

        }
    }

    void pushArray(int n) {
        T e;
        for (int i = 0; i < n; i++) {
            cout << "Enter elem:";
            cin >> e;
            container.push_back(e);

        }

    }
};


#endif //STACK_STACK_H
